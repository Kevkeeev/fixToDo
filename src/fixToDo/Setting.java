package fixToDo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.json.*;

public class Setting {
	private int loglevel;
	private String settingsPath;
	private ArrayList<Project> projects;
	
	public Setting() { this(0, ""); }
	public Setting(int loglevel) { this( loglevel, ""); }
	public Setting(String settingsPath) { this( 0, settingsPath); }

	public Setting( int loglevel, String settingsPath) {
		this.loglevel = loglevel;
		this.settingsPath = settingsPath;
		this.projects = new ArrayList<>();
		
		if( ! this.settingsPath.isEmpty() ) {
			try {
				this.loadFromFile();
			} catch (JSONException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
	}
	
	public int getLoglevel() { return this.loglevel; }
	public void setLoglevel( int newLevel ) { this.loglevel = newLevel; }
	
	public String getSettingspath() { 
		if( this.settingsPath.isEmpty()) { askSettingspath(); }
		return this.settingsPath; 
	}
	public void setSettingspath( String newPath ) { this.settingsPath = newPath; }
	
	public ArrayList<Project> getProjects() { return this.projects; }
	public void setProjects( ArrayList<Project> newProjects ) { this.projects = newProjects; }
	public void addProject( Project newProject ) { this.projects.add( newProject ); }
	public void removeProject( Project removeProject ) { this.projects.remove( removeProject ); }
	
	public void askSettingspath() {
		String newPath = "";
		
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File( 
			System.getProperty("user.home") + File.separator + "Dropbox" + File.separator + "Apps" + 
				File.separator + "ShareLaTeX" + File.separator + "."
		));
		chooser.setDialogTitle("Select the Settings file");
		chooser.setFileFilter(new FileNameExtensionFilter(".json files", "json"));
        chooser.setAcceptAllFileFilterUsed(false);

        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            newPath = chooser.getSelectedFile().getAbsolutePath();

            System.out.println("selected path: " + newPath);
            if(! newPath.endsWith(".json") && newPath.endsWith( File.separator )) {
            	newPath += "fixToDo.json"; //if directory selected, add filename to the end.
            }
        }
	}
	
	public String toJSON() {
		String json = "{" 
			+ "\"loglevel\": " + this.loglevel + ", "
			+ "\"projects\": [";
		
		for(Project p : this.projects) {
			json += p.toJSON() + ",";
		}

		json = json.substring(0, json.length() - 1) + "] }";
		return json;
	}
	
	public void loadFromFile() throws JSONException {
		BufferedReader br = null;
		FileReader fr = null;
		String line;
		String jsonText = "";

		try {
			fr = new FileReader( this.getSettingspath() );
			br = new BufferedReader(fr);

			while ((line = br.readLine()) != null) {
				jsonText += line;
			}
			
		} catch (IOException e) {
			e.printStackTrace();

		} finally {
			try {
				if (br != null) { br.close(); }
				if (fr != null) { fr.close(); }
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		if(! jsonText.isEmpty() ) {
			JSONTokener tokener = new JSONTokener( jsonText );
			JSONObject settings = new JSONObject(tokener);
			JSONArray jsonProjects = new JSONArray();
			
			if(settings.has("loglevel")) { this.loglevel = (int) settings.get("loglevel"); }
			if(settings.has("projects")) { jsonProjects = (JSONArray) settings.get("projects"); }
			
			for(int i=0; i<jsonProjects.length(); i++) {
				this.addProject( new Project(
					(String) ((JSONObject) jsonProjects.get(i)).get("filepath"),
					(String) ((JSONObject) jsonProjects.get(i)).get("todofile")
				) );
			}
		}
	}
	
	public void saveToFile() throws JSONException {
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter( this.getSettingspath() );
			bw = new BufferedWriter(fw);
			bw.write( this.toJSON() );
		} catch (IOException e) {
			e.printStackTrace();

		} finally {
			try {
				if (bw != null) { bw.close(); }
				if (fw != null) { fw.close(); }
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
}
