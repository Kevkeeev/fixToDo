package fixToDo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;

import org.json.JSONException;

public class FixToDo {
	
	private static Setting settings;
	
	public static void DEBUG( String msg ) { DEBUG(msg, 0); }
	public static void DEBUG( String msg, int lvl ) {
		if(lvl <= settings.getLoglevel()) { System.out.println("[DBG:" + lvl + "] " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " > " + msg); }
	}
	
	public static void main(String[] args) {
		settings = new Setting();
		loadArgs( Arrays.asList(args) );
		
		askProjects();
		if( settings.getProjects().size() <= 0) {
			System.exit(0); //if after asking to add projects, still nothing added, then simply exit.
		}
		
		ArrayList<String> files;
		ArrayList<String> todos;
		
		for(Project project : settings.getProjects() ) {
			files = findFiles( project.getFilepath() );
			todos = new ArrayList<>();
			
			for(String file : files) {
				ArrayList<String> tmp = (file == project.getTodofile() ? new ArrayList<>() : findTodos(file));
				todos.addAll( tmp  );
				if( tmp.size() > 0 && files.get( files.size() - 1) != file ) {
					todos.add("  ");//empty separator
				} // only if not last file, and file had todo's
			}
			
			writeTodosToFile(todos, project.getFilepath() + File.separator + project.getTodofile());
		}
		
		try {
			settings.saveToFile();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public static void loadArgs( List<String> args) {
		for(String arg : args) {
			if( arg.toLowerCase().substring( 0, arg.indexOf("=") ).equals("loglevel") ) {
				settings.setLoglevel( Integer.parseInt( arg.substring( arg.indexOf("=")+1 ) ) );
				if(settings.getLoglevel() < 0) { settings.setLoglevel( settings.getLoglevel() * -1 ); }
				break;
			}
		} //first read loglevel if given anywhere.
		
		for(String arg : args) {
			switch(arg.toLowerCase().substring(0, arg.indexOf("="))) {
				case "loglevel" : break; //already parsed
				
				case "settings" : 
					settings.setSettingspath( arg.substring( arg.indexOf("=") +1) );
					break;
					
				default: 
					DEBUG("Unsupported argument: " + arg.substring(0, arg.indexOf("=")), 0);
			}
		}
		try {
			settings.loadFromFile();
			DEBUG("Loaded settings: \n" + settings.toJSON() , 1);
		} catch (JSONException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
	}
	
	public static void askProjects(){
		Project tmp;
		
		while( true ) {
			tmp = new Project();

			int askNewProject = JOptionPane.showConfirmDialog(
				null, //parent object
				"Do you want to add another project?", //body question
				"Add another project?", //title
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE
			);
			if(askNewProject == JOptionPane.YES_OPTION){

				tmp.setFilepath( Project.askFilepath( 
					System.getProperty("user.home") + File.separator + "Dropbox" + File.separator + "Apps" + 
						File.separator + "ShareLaTeX" + File.separator + "."
				));
				tmp.setTodofile( Project.askTodofile( tmp.getFilepath() ));
				
				DEBUG("Adding new Project: " + tmp.getFilepath() + " - " + tmp.getTodofile(), 1);
				settings.addProject(tmp);
			} else {
				break; //out of the loop.
			}
		}
		
	}
	
	public static ArrayList<String> findFiles ( String filepath ) {
		ArrayList<String> files = new ArrayList<>();
		
		File f = new File(filepath);
		File[] matchingFiles = f.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.endsWith(".tex");
		    }
		});
		for(File file : Arrays.asList(matchingFiles) ) {
			files.add( file.getAbsolutePath() );
		}
		
		return files;
	}

	public static ArrayList<String> findTodos ( String filepath) {
		DEBUG("Find ToDos: " + filepath, 2);
		int linenr = 1;
		int ignore = 0;
		BufferedReader br = null;
		FileReader fr = null;
		String line;
		ArrayList<String> todos = new ArrayList<String>();

		try {
			fr = new FileReader(filepath);
			br = new BufferedReader(fr);

			while ((line = br.readLine()) != null) {
				DEBUG(linenr + ": " + line, 4);
				if( line.contains("\\iffalse")) 		{ ignore++; DEBUG(linenr + ": " + line, 2); }
				if( line.contains("\\fi") && ignore>0)	{ ignore--; DEBUG(linenr + ": " + line, 2); }

				if( ignore <= 0 || line.contains("\\iffalse") ) {
					if( line.contains("%TODO") ) {
						DEBUG(linenr + ": " + line, 3);
						line = line.substring( line.indexOf("%TODO") + 6 );
						todos.add(filepath.substring( filepath.lastIndexOf(File.separator)+1, filepath.indexOf("_")) + ":" + linenr + " " + line);
					}
				}
				linenr++;
			}
			
		} catch (IOException e) {
			e.printStackTrace();

		} finally {
			try {
				if (br != null) { br.close(); }
				if (fr != null) { fr.close(); }
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return todos;
	}
	
	public static void writeTodosToFile ( ArrayList<String> todos, String filepath ) {
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(filepath);
			bw = new BufferedWriter(fw);

			bw.write("\\begin{itemize}\n");
			for(String todo : todos) {
				bw.write( "\t\\item[" + todo.substring(0, todo.indexOf(" ")) + "] " + todo.substring(todo.indexOf(" ") + 1)  + "\n");
			}
			bw.write("\\end{itemize}\n");

		} catch (IOException e) {
			e.printStackTrace();

		} finally {
			try {
				if (bw != null) { bw.close(); }
				if (fw != null) { fw.close(); }
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
}
