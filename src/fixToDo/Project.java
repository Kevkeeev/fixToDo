package fixToDo;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Project {

	private String	filepath;
	public void		setFilepath(String newPath) { this.filepath = newPath; }
	public String	getFilepath() { return this.filepath; }
	
	private String	todofile;
	public void		setTodofile(String newFile) { this.todofile = newFile; }
	public String	getTodofile() { return this.todofile; }
	
	public Project() {}
	public Project(String newPath, String newFile) {
		this.filepath = newPath;
		this.todofile = newFile;
	}

	public String toJSON() {
		return 
		"{" +
			"\"filepath\": \"" + this.filepath.replace("\\", "\\\\") + "\"," + //escape all 
			"\"todofile\": \"" + this.todofile + "\"" +
		"}";
	}
	
	public static String askFilepath( String filepath ) {
		
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File( filepath ));
		chooser.setDialogTitle("Select the Project to fixToDo");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
        	filepath = chooser.getSelectedFile().getAbsolutePath();
        } else {
        	System.exit(0); //user cancelled, close program.
        }
		
		return filepath;
	}
	
	public static String askTodofile( String filepath ) {
		String todoFile = "";
		
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File( filepath ));
		chooser.setDialogTitle("Select the ToDo file");
		chooser.setFileFilter(new FileNameExtensionFilter(".tex files", "tex"));
        chooser.setAcceptAllFileFilterUsed(false);

        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            todoFile = chooser.getSelectedFile().getName();
        } else {
        	System.exit(0); //user cancelled, close program.
        }

        return todoFile;
	}
	
}
